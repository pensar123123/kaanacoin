Building Kaanacoin
================

See doc/build-*.md for instructions on building the various
elements of the Kaanacoin Core reference implementation of Kaanacoin.
